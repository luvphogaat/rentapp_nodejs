const stripe = require('stripe')
const STRIPE_SECRET_KEY = 'sk_test_51IKMi9Jgdw2bw88H7iwU7rlooTYS8wQcl0ESJJsgeaPRJDnDfeUGxcLkQa4So6J0VHX3ymjUyaOpqH6mp0SWN6Jg00jD5iJi3w'

const Stripe = stripe(STRIPE_SECRET_KEY, {
  apiVersion: '2020-08-27'
})

const addNewCustomer = async (email) => {
  const customer = await Stripe.customers.create({
    email,
    description: 'New Customer'
  })

  return customer
}

const getCustomerByID = async (id) => {
  const customer = await Stripe.customers.retrieve(id)
  return customer
}

module.exports = {
  addNewCustomer,
	getCustomerByID
}