const express = require('express')
const cors = require('cors');
const loginRouter = require('../routes/login')
const roomRouter = require('../routes/room');
const tenantRouter = require('../routes/tenant');
const ownerRouter = require('../routes/owner');
const smsRouter = require('../routes/sms');
const dashboardRouter = require('../routes/dashboard');
const roomList = require('../jsonData/roomList.json');
const app = express()
const http = require('http').Server(app);
const io = require('socket.io')(http, {cors: {origin: '*'}});
const port = process.env.PORT || 3001;

app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization, Access-Control-Allow-Origin, Access-Control-Allow-Headers');
    next();
});
// const io = socketIO(server, {
//     transport: ['polling'],
//     cors: {
//         cors: {
//             origin: "http://localhost:3000"
//         }
//     }
// })
const db = require('../models/index');
// const { emit } = require('process');
const {owner_details, PropertyDetails, PropertyImages, chatHistory} = db;
const history = [];
const users = [];

// io.on('connection', (socket) => {
//     console.log('Socket ID ==> ' , socket.id)
//     const username = socket.handshake.auth.username;
//     let userId = '';
//     console.log('Token', username, 'User id', userId );
//     const users = [];
//     for (let [id, socket] of io.of("/").sockets) {
//         users.push({
//             userID: id,
//             username: socket.handshake.auth.username,
//         });
//     }
//     socket.emit("users", users);
//     console.log('Users List', users);
//     socket.on('disconnect', function(){
//         io.emit('users-changed', {user: socket.name, event: 'left'});   
//     });

//     socket.broadcast.emit("user connected", {
//         userID: socket.id,
//         username: socket.handshake.auth.username,
//     });

//     socket.on('my message', (msg) => {
//         console.log('message: ' + msg);
//     });


    
    // socket.on('start-chat', (chatId) => {
    //     // socket.chatId = chatId;
    //     // socket.join(chatId);
    //     users.push({id : socket.client.id, username: socket.name});
    //     console.log(users);
    // });
    // socket.on('set-name', name => {
    //     socket.name = name;
    //     io.emit('users-changed', {user: name, event: 'joined'});
    // });
    
    // socket.on('add-message', (message) => {
    //     io.emit('message', {text: message.text, from: socket.name, created: new Date()});
    // });

    // socket.on("private message", (data) => {
    //     console.log(data.anotherSocketId, 'Socket ID', data.msg);
    //     debugger;
    //     socket.to(data.anotherSocketId).emit("private message", socket.id, data.msg);
    // });
    // socket.emit('crash', {socket:socket});
//     socket.broadcast.emit("user connected", {
//         userID: socket.id,
//         username: username,
//     });

//     socket.on("private message", ({ content, to }) => {
//         console.log('Private Chat Entered', to ,' with message ', content);
//         socket.to(to).emit("private message", {
//           content,
//           from: socket.id,
//         });
//     });
// });

// io.on("connection", (socket) => {
//     const users = [];
//     for (let [id, socket] of io.of("/").sockets) {
//       users.push({
//         userID: id,
//         username: socket.username,
//       });
//     }
//     socket.emit("users", users);
//     // ...
// });


const router = express.Router();
app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(cors());

app.get('/v1/vacateList/', (req,res) => {
    try {
        PropertyDetails.findAll(
            {
                where: {
                    availableStatus: 1
                },
                order: ['propertyNumber', 'propertyArea', 'updatedAt'],
                include: [{
                    model:PropertyImages,
                    attributes: ['smallImage'],
                },{
                    model: owner_details,
                    attributes: ['name', 'primaryNumber', 'secondaryNumber'],
                }]
            }).then(data => {
                res.json(data);
            });
    } catch(e) {
        res.json(e);
    }
    
});


app.use('/v1/login', loginRouter);

//Room Related activity
app.use('/v1/room', roomRouter);

// Tenant Related Activity
app.use('/v1/tenant', tenantRouter);

// Owners Related Activity
app.use('/v1/owner', ownerRouter);

// app.use('/v1/sms', smsRouter);

app.use('/v1/dashboard', dashboardRouter);

app.use(router);  




http.listen(port, () => {
    console.log('Server is up and running on port ' + port);
});