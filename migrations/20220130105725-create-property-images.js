'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PropertyImages', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      propertyId: {
        allowNull: false,
        type: Sequelize.UUID
      },
      image: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      smallImage: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      thumbnail: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PropertyImages');
  }
};