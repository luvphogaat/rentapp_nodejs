'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PropertyRents', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      ownerId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      propertyId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      tenantId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      rentAmount: {
        type: Sequelize.DOUBLE
      },
      balance: {
        type: Sequelize.DOUBLE
      },
      advance: {
        type: Sequelize.DOUBLE
      },
      month: {
        type: Sequelize.STRING
      },
      year: {
        type: Sequelize.STRING
      },
      pay_mode: {
        type: Sequelize.STRING
      },
      pay_status: {
        type: Sequelize.ENUM,
        values: ["PAID", "UNPAID"],
        defaultValue: "UNPAID",
        allowNull: false,
      },
      pay_dest: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PropertyRents');
  }
};