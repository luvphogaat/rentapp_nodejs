"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("TenantPropertyLinks", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      ownerId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      propertyId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      tenantId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      startDate: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      endDate: {
        type: Sequelize.DATE,
      },
      rent_amount: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      security: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      security_deposit_amount: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      security_amount_date: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      security_return_amount: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      security_return_date: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      status: {
        type: Sequelize.ENUM,
        values: ["active", "inactive"],
        defaultValue: "active",
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("TenantPropertyLinks");
  },
};
