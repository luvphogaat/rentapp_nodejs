'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('tenantDetails', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      name: {
        type: Sequelize.STRING
      },
      plan: {
        type: Sequelize.STRING
      },
      permanentAddress: {
        type: Sequelize.TEXT
      },
      primaryNumber: {
        type: Sequelize.STRING
      },
      primaryNumberPrefix: {
        type: Sequelize.STRING
      },
      secondaryNumber: {
        type: Sequelize.STRING
      },
      secondaryNumberPrefix: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tenantDetails');
  }
};