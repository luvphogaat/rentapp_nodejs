'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('tenantPoliceVerifications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tenantId: {
        type: Sequelize.UUID
      },
      image: {
        type: Sequelize.TEXT
      },
      smallImage: {
        type: Sequelize.TEXT
      },
      thumnail: {
        type: Sequelize.TEXT
      },
      verificationNumber: {
        type: Sequelize.STRING
      },
      latest: {
        type: Sequelize.ENUM,
        values: ["true","false"],
        defaultValue: "true",
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tenantPoliceVerifications');
  }
};