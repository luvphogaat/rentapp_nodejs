'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PropertyMiscBills', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      ownerId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      propertyId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      tenantId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      type: {
        type: Sequelize.ENUM,
        values: ["WATER", "CLEANING"],
        defaultValue: "WATER",
        allowNull: false,
      },
      month: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      year: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      amount: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      pay_mode: {
        type: Sequelize.STRING,
        allowNull: true
      },
      pay_status: {
        type: Sequelize.ENUM,
        values: ["PAID", "UNPAID"],
        defaultValue: "UNPAID",
        allowNull: false,
      },
      pay_dest: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PropertyMiscBills');
  }
};