'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PropertyRentAgreements', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      ownerId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      propertyId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      tenantId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      certificateNumber: {
        type: Sequelize.STRING
      },
      certificateDateTime: {
        type: Sequelize.STRING
      },
      FirstParty: {
        type: Sequelize.STRING
      },
      secondParty: {
        type: Sequelize.STRING
      },
      startDate: {
        type: Sequelize.DATE
      },
      endDate: {
        type: Sequelize.DATE
      },
      image: {
        type:Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PropertyRentAgreements');
  }
};