'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('PropertyDetails', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      ownerId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      locationArea: {
        type: Sequelize.STRING
      },
      propertyArea: {
        type: Sequelize.STRING
      },
      propertyNumber: {
        type: Sequelize.STRING
      },
      floor: {
        type: Sequelize.STRING
      },
      roomNumber: {
        type: Sequelize.INTEGER
      },
      purpose: {
        type: Sequelize.STRING
      },
      bhkType: {
        type: Sequelize.STRING
      },
      rentAmount: {
        type: Sequelize.INTEGER
      },
      depositAmount: {
        type: Sequelize.INTEGER
      },
      water: {
        type: Sequelize.INTEGER
      },
      maintenance: {
        type: Sequelize.INTEGER
      },
      maintenanceType: {
        type: Sequelize.STRING
      },
      electricityType: {
        type: Sequelize.STRING
      },
      electricityPerUnit: {
        type: Sequelize.STRING
      },
      furnishedStatus: {
        type: Sequelize.STRING
      },
      category: {
        type: Sequelize.STRING
      },
      bathroom: {
        type: Sequelize.INTEGER
      },
      kitchen: {
        type: Sequelize.INTEGER
      },
      balcony: {
        type: Sequelize.INTEGER
      },
      availableStatus: {
        type: Sequelize.INTEGER
      },
      availableDate: {
        type: Sequelize.STRING
      },
      society: {
        type: Sequelize.BOOLEAN
      },
      district: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('PropertyDetails');
  }
};