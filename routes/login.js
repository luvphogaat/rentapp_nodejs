const express = require('express');
const db = require('../models/index')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const { secretKey } = require('../config/config.json');

const bcrypt = require('bcrypt');

// Passport
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const saltRounds = 10;
// const Stripe = require('../src/connect/stripe')



//Database reference
const {User, owner_details, tenantDetails, profilePicture, tenantPoliceVerification } = db;

const router = express.Router();

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = secretKey;
// opts.issuer = 'webapi.gangarent.in';
// opts.audience = 'gangarent.in';

// passport authentication
passport.use(
    'clientLocal',
    new LocalStrategy((username, password, next) => {
      User.findOne({ where: { email: username }, raw: false })
        .then((user) => {
            console.log('credentials passed to passport' + username + '' + password)
          if (!user) {
            return next(null, false, { message: 'Incorrect username.' });
          }
          if (!bcrypt.compareSync(password, user.password)) {
            return next(null, false, { message: 'Incorrect password.' });
          }
          return next(null, user);
        })
        .catch((err) => (null, false, err));
    })
  );

passport.use(
    'clientJwt',
    new JwtStrategy(opts, (jwtPayload, done) => {
      User.findOne({ where: { id: jwtPayload.id }, raw: false })
        .then((user) => {
          if (!user) {
            return done(null, false, { message: 'Incorrect user.' });
          }
          return done(null, user);
        })
        .catch((err) => (null, false, err));
    })
);

// Register
// Register API
router.post('/register', jsonParser, (req, res) => {
    if (req.body.email && req.body.password) {
      User.findOne({ where: { email: req.body.email }, raw: false })
        .then(async (user) => {
          if (user) {
            res.status(401).json({ message: 'Username already exists' });
          } else {
            const hash = bcrypt.hashSync(req.body.password, saltRounds);
            if(req.body.type === 'tenant'){
              const isTenantExist = await tenantDetails.findOne({where: {primaryNumber: req.body.phone}});
              console.log(isTenantExist);
              if(isTenantExist) {
                
                User.create({
                  email: req.body.email,
                  password: hash,
                  firstName: req.body.firstName,
                  lastName: req.body.lastName,
                  phone: req.body.phone,
                  type: req.body.type,
                  uniqueId: isTenantExist.id
                  // stripeId: req.body.stripeId,
                }).then((userNew) => {
                  const payload = { id: userNew.id };
                  const token = jwt.sign(payload, secretKey);
                  return res.json({ 
                    access_token: token,
                    role: userNew.type,
                    email: userNew.email,
                    plan: 'free',
                    firstName: userNew.firstName,
                    lastName: userNew.lastName
                  });
                })
              } else {
                tenantDetails.create({
                  name: req.body.firstName + ' ' + req.body.lastName,
                  plan: 'free',
                  permanentAddress: '',
                  primaryNumber: req.body.phone,
                  gender: 'Male'
                }).then((data) => { 
                  User.create({
                    email: req.body.email,
                    password: hash,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    phone: req.body.phone,
                    type: req.body.type,
                    uniqueId: data.id
                    // stripeId: req.body.stripeId,
                  }).then((userNew) => {
                    const payload = { id: userNew.id };
                    const token = jwt.sign(payload, secretKey);
                    return res.json({ 
                      access_token: token,
                      role: userNew.type,
                      email: userNew.email,
                      plan: 'free',
                      firstName: userNew.firstName,
                      lastName: userNew.lastName
                    });
                  })
                  .catch((err) => {
                    res.status(401).json({ message: 'Error Creating User -' + err });
                  });
                })
              }
            } else {
              User.create({
                email: req.body.email,
                password: hash,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                phone: req.body.phone,
                type: req.body.type,
                uniqueId: req.body.uniqueId
                // stripeId: req.body.stripeId,
              })
                .then((userNew) => {
                  const payload = { id: userNew.id };
                  const token = jwt.sign(payload, secretKey);
                  return res.json({ 
                    access_token: token,
                    role: userNew.type,
                    email: userNew.email,
                    plan: 'free',
                    firstName: userNew.firstName,
                    lastName: userNew.lastName
                  });
                })
                .catch((err) => {
                  res.status(401).json({ message: 'Error Creating User -' + err });
                });
            }
            
          }
        })
        .catch((err) => {
          res.status(401).json({ message: err + 'here it is ' });
        });
    } else {
      res.status(401).json({ message: 'Insufficient Information to register' });
    }
});

// Login API 
router.post('/login', jsonParser, (req, res, next) => {
    passport.authenticate('clientLocal', async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
    //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json({ success: false, info, data: req.body });
      }
      req.login(user, (loginErr) => {
        if (loginErr) {
          return next(loginErr);
        }
        const payload = { id: req.user.id };
        const token = jwt.sign(payload, secretKey);
        // Stripe.addNewCustomer(user.email).then(customer => {
        //   return res.send('Customer created: ' + JSON.stringify(customer))
        // })
        return res.json({ 
          access_token: token,
          role: user.type,
          email: user.email,
          plan: 'free',
          firstName: user.firstName,
          lastName: user.lastName
        });
      });
    })(req, res, next);
});

// Get User API
// router.get(
//     '/:id',
//     passport.authenticate('clientJwt', { session: false }), (req, res) => {
//       const clientId = req.params.id;
//       return User.findOne({
//         where: {
//           id: clientId,
//         },
//         raw: false,
//       }).then((result) => {
//           if (!result){
//             res.send('Not Found');
//           }
//         res.send(result);
//       }).catch(err => {
//         res.send(err);
//       });
//     }
// );

router.get('/settings/', jsonParser, (req, res, next) => {
  passport.authenticate(['ownerJwt', 'tenantJwt'], (err, user, info) => {
      if (err) {
          return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
          return res.status(401).json('Unauthorized');
      }
      try {
        if ( user.type === 'owner' ){
          owner_details.findOne(
            {
              where: {
                  id: user.uniqueId
              },
              include: [
                {
                  model: profilePicture,
                  attributes: ['small'],
                  required: false
                }
              ]
            }).then(data => {
                res.json(data);
            });
        } else if ( user.type === 'tenant' ) {
            tenantDetails.findOne(
              {
                where: {
                    id: user.uniqueId
                },
                include: [
                  {
                    model: profilePicture,
                    attributes: ['small'],
                    required: false
                  },  {
                    model: tenantPoliceVerification,
                    required: false
                  }
                ]
              }).then(data => {
                  res.json({...data.dataValues, role: user.role});
              });
          }
      } catch(e) {
          res.json(e);
      }
  }, {session: false})(req, res, next);
});



passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));

module.exports = router;