const express = require("express");
// const url = require('url');
const db = require("../models/index");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const passport = require("passport");
const roomList = require("../jsonData/roomList.json");
const { v4: uuidv4 } = require("uuid");
require("./passport");
const roomSearchFilters = require("../jsonData/roomSearchCreteriaFilter.json");
const monthList = require('../jsonData/monthList');

//Database reference
const {
  owner_details,
  PropertyDetails,
  PropertyImages,
  tenantDetails,
  TenantPropertyLinks,
  PropertyRent,
  PropertyElectricityBills,
  PropertyMiscBills,
  PropertyRentAgreement,
  tenantPoliceVerification,
  profilePicture
} = db;

const router = express.Router();

router.get("/list", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        PropertyDetails.findAll({
          where: {
            ownerId: user.uniqueId,
          },
          order: ["propertyNumber", "propertyArea"],
          include: [
            {
              model: PropertyImages,
              attributes: ["smallImage"],
            },
            {
              model: owner_details,
              attributes: ["name", "primaryNumber", "secondaryNumber"],
            },
            {
              model: tenantDetails
            },
          ],
        }).then((data) => {
          res.json(data);
        });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// Vacate Room Details
router.get("/list/available/", jsonParser, (req, res, next) => {
  console.log(req.header);
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        PropertyDetails.findAll({
          where: {
            availableStatus: 1,
          },
          include: [
            {
              model: PropertyImages,
              attributes: ["smallImage"],
            },
            {
              model: owner_details,
              attributes: ["name", "primaryNumber", "secondaryNumber"],
            },
          ],
        })
          .then((data) => {
            res.json(data);
          })
          .catch((err) => {
            res.status(400).send(err);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// Single Room Details
router.get("/list/:id/single", jsonParser, (req, res, next) => {
  console.log(req.header);
  passport.authenticate(
    ["ownerJwt", "tenantJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        PropertyDetails.findByPk(req.params.id, {
          include: [
            {
              model: PropertyImages,
              attributes: ["smallImage"],
              required: false,
            },
            {
              model: owner_details,
              attributes: ["name", "primaryNumber", "secondaryNumber"],
            },
            {
              model: tenantDetails,
              through: {
                order: ["status", "ASC"]
              },
              required: false,
            },
            {
              model: PropertyRent,
              order: [
                ["month", "DESC"],
                ["year", "DESC"]
              ],
              required: false,
              separate: true
            },
            {
              model: PropertyElectricityBills,
              order: [
                ["month", "DESC"],
                ["year", "DESC"]
              ],
              required: false,
              separate: true
            }, {
              model: PropertyMiscBills,
              order: [
                ["month", "DESC"],
                ["year", "DESC"]
              ],
              required: false,
              separate: true
            }
          ],
          order: [
            [
              { model: tenantDetails.TenantPropertyLinks }, 'status', 'ASC'
            ]
          ]
        })
          .then(async (data) => {
            const active_tenant = await TenantPropertyLinks.findOne({
              where: {
                status: 'active',
                propertyId: data.id,
                ownerId: user.uniqueId
              }
            });
            console.log(active_tenant);
            res.json({ ...data.dataValues, active_tenant: active_tenant });
          })
          .catch((err) => {
            res.status(400).send(err);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.post("/link/:propertyid", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        console.log("reached here");
        TenantPropertyLinks.create({
          id: uuidv4(),
          ownerId: user.uniqueId,
          propertyId: req.params.propertyid,
          tenantId: req.body.tenantId,
          startDate: req.body.startDate,
          endData: req.body.endDate,
          rent_amount: req.body.rent_amount,
          security: req.body.security,
          security_deposit_amount: req.body.security_deposit_amount,
          security_amount_date: req.body.security_amount_date,
          pay_date: req.body.pay_day,
          status: "active",
        })
          .then(async (data) => {
            await PropertyDetails.update(
              {
                availableStatus: 0,
              },
              {
                where: {
                  ownerId: user.uniqueId,
                  id: req.params.propertyid,
                },
              }
            );
            console.log("also reached here");
            res.status(201).json(data);
          })
          .catch((err) => {
            console.log("also reached here  error");
            res.json(err);
          });
      } catch (e) {
        console.log("error exceptional catch", e);
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.get("/list/vacate/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        PropertyDetails.findAll({
          where: {
            ownerId: user.uniqueId,
          },
          include: [
            {
              model: PropertyImages,
              attributes: ["smallImage"],
            },
            {
              model: owner_details,
              attributes: ["name", "primaryNumber", "secondaryNumber"],
            },
            {
              model: tenantDetails,
              required: false,
            },
          ],
        }).then((data) => {
          const getFilteredList = data.filter(
            (user) => user.tenantDetails.length == 0
          );
          res.json(getFilteredList);
        });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.get("/search/filterList/", (req, res) => {
  try {
    res.json(roomSearchFilters);
  } catch (e) {
    res.json(e);
  }
});

router.post("/new/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        PropertyDetails.create({
          id: uuidv4(),
          ownerId: "",
          locationArea: "",
          propertyArea: "",
          propertyNumber: "",
          floor: "",
          roomNumber: "",
          purpose: "",
          bhkType: "",
          rentAmount: "",
          depositAmount: "",
          water: "",
          maintenance: "",
          maintenanceType: "",
          electricityType: "",
          electricityPerUnit: "",
          furnishedStatus: "",
          category: "",
          bathroom: "",
          kitchen: "",
          balcony: "",
          availableStatus: "",
          availableDate: "",
          society: "",
          district: "",
          state: "",
        }).then(() => {
          PropertyImages.create({
            id: "",
            propertyId: "",
            image: "",
            smallImage: "",
            thumbnail: "",
          }).then(() => { });
        });
        res.status(201).json(req.body);
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// updating the Room Details
router.patch("/edit/:propertyid", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const ownerid = user.uniqueId;
        PropertyDetails.findOne({ where: { id: req.params.propertyid } }).then(
          (property) => {
            if (property) {
              property
                .update({
                  ownerId: ownerid,
                  locationArea: req.body.locationArea,
                  propertyArea: req.body.propertyArea,
                  propertyNumber: req.body.propertyNumber,
                  floor: req.body.floor,
                  roomNumber: req.body.roomNumber,
                  purpose: req.body.purpose,
                  bhkType: req.body.bhkType,
                  rentAmount: req.body.rentAmount,
                  depositAmount: req.body.depositAmount,
                  water: req.body.water,
                  maintenance: req.body.maintenance,
                  maintenanceType: req.body.maintenanceType,
                  electricityType: req.body.electricityType,
                  electricityPerUnit: req.body.electricityPerUnit,
                  furnishedStatus: req.body.furnishedStatus,
                  category: req.body.category,
                  bathroom: req.body.bathroom,
                  kitchen: req.body.kitchen,
                  balcony: req.body.balcony,
                  availableStatus: req.body.avaiableStatus,
                  availableDate: req.body.availableDate,
                  society: req.body.society,
                  district: req.body.district,
                  state: req.body.state,
                })
                .then(() => {
                  // PropertyImages.create({
                  //     id: '',
                  //     propertyId: '',
                  //     image: '',
                  //     smallImage: '',
                  //     thumbnail: ''
                  // }).then(() => {})
                });
            }
          }
        );
        res.status(201).json(req.body);
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// Unlinking Tenant and Property and storing them in history
router.post("/unlink/:propertyid/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        console.log("reached here");
        // TenantPropertyLinks.create({
        //     id: uuidv4(),
        //     propertyId: req.params.propertyid,
        //     tenantId: req.body.tenantId,
        //     startDate: req.body.startDate,
        //     endData: req.body.endDate,
        //     status: "active",
        // }).then(data => {
        //     console.log('also reached here');
        //     res.status(201).json(data);
        // }).catch(err => {
        //     console.log('also reached here  error');
        //     res.json(err);
        // })
        TenantPropertyLinks.findOne({
          where: {
            propertyId: req.params.propertyid,
            tenantId: req.body.tenantId,
          },
        }).then((details) => {
          details.status = "inactive";
          details.endDate = req.body.availabilityFrom;
          details.comment = req.body.comment;
          details.save();
          PropertyDetails.findByPk(req.params.propertyid)
            .then((propertydata) => {
              propertydata.availableStatus = 1;
              propertydata.availableDate = req.body.availabilityFrom;
              propertydata.save();
              // console.log(details);
              // console.log(propertydata);
              res.status(200).json("success");
            })
            .catch((err) => {
              res.status(400).send(err);
            });
        });
      } catch (e) {
        console.log("error exceptional catch", e);
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// Guest can post new Property
router.post("/post/new/", (req, res) => {
  try {
    // return res.json(req.body);
    owner_details
      .findOne({
        where: { name: req.body.name, primaryNumber: req.body.primary },
      })
      .then((details) => {
        if (details === null) {
          owner_details
            .create({
              id: uuidv4(),
              name: req.body.name,
              aadharCard: req.body.aadharCard,
              fatherName: req.body.father,
              primaryNumber: req.body.primary,
              secondaryNumber: req.body.secondary,
            })
            .then((ownerdetails) => {
              PropertyDetails.create({
                id: uuidv4(),
                ownerId: ownerdetails.id,
                locationArea: req.body.roomLocation,
                propertyArea: req.body.roomArea,
                propertyNumber: req.body.propertyNumber,
                floor: req.body.roomFloor,
                roomNumber: req.body.roomNumber,
                purpose: req.body.roomPurpose,
                bhkType: req.body.roomtype,
                rentAmount: req.body.rent,
                depositAmount: req.body.security,
                water: req.body.waterCharges,
                maintenance: req.body.maintenanceAmount,
                maintenanceType: req.body.maintenanceType,
                electricityType: req.body.meterType,
                electricityPerUnit: req.body.electricityPerUnit,
                furnishedStatus: req.body.furnishedStatus,
                category: req.body.category,
                bathroom: req.body.bathroom,
                kitchen: req.body.kitchen,
                balcony: req.body.balcony,
                availableStatus: req.body.avaiableStatus,
                availableDate: req.body.availableFrom,
                society: req.body.society,
                district: req.body.district,
                state: req.body.state,
              }).then(() => {
                // PropertyImages.create({
                //     id: '',
                //     propertyId: '',
                //     image: '',
                //     smallImage: '',
                //     thumbnail: ''
                // }).then(() => {
                //     res
                // })
                res.status(201).json("created");
              });
            });
        } else {
          PropertyDetails.create({
            id: uuidv4(),
            ownerId: details.id,
            locationArea: req.body.roomLocation,
            propertyArea: req.body.roomArea,
            propertyNumber: req.body.propertyNumber,
            floor: req.body.roomFloor,
            roomNumber: req.body.roomNumber,
            purpose: req.body.roomPurpose,
            bhkType: req.body.roomtype,
            rentAmount: req.body.rent,
            depositAmount: req.body.security,
            water: req.body.waterCharges,
            maintenance: req.body.maintenanceAmount,
            maintenanceType: req.body.maintenanceType,
            electricityType: req.body.electricityType,
            electricityPerUnit: req.body.electricityPerUnit,
            furnishedStatus: req.body.furnishedStatus,
            category: req.body.category,
            bathroom: req.body.bathroom,
            kitchen: req.body.kitchen,
            balcony: req.body.balcony,
            availableStatus: req.body.avaiableStatus,
            availableDate: req.body.availableFrom,
            society: req.body.society,
            district: req.body.district,
            state: req.body.state,
          }).then(() => {
            // PropertyImages.create({
            //     id: '',
            //     propertyId: '',
            //     image: '',
            //     smallImage: '',
            //     thumbnail: ''
            // }).then(() => {
            //     res
            // })
            res.status(201).json("created");
          });
        }
      });
  } catch (e) {
    res.json(e);
  }
});

router.post("/collect/rent/:propertyid", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const check_if_recordexist = await PropertyRent.count({
          where: {
            ownerId: user.uniqueId,
            propertyId: req.params.propertyid,
            tenantId: req.body.tenantId,
            month: req.body.month,
            year: req.body.year
          }
        });
        console.log(check_if_recordexist)
        if (check_if_recordexist > 0) {
          return res.status(400).json("Rent already Exist");
        }
        PropertyRent.create({
          id: uuidv4(),
          ownerId: user.uniqueId,
          propertyId: req.params.propertyid,
          tenantId: req.body.tenantId,
          rentAmount: req.body.rentRecd,
          month: req.body.month,
          year: req.body.year,
          advance: req.body.advance,
          balance: req.body.balance,
          pay_status: req.body.pay_status,
          pay_mode: req.body.pay_mode,
        })
          .then((data) => {
            res.status(201).json(data);
          })
          .catch((err) => {
            res.json(err);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.patch("/update/rent/:rentid", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const check_if_recordexist = await PropertyRent.count({
          where: {
            ownerId: user.uniqueId,
            id: req.params.rentid
          }
        });
        if (check_if_recordexist == 0) {
          return res.status(400).json("Rent does not Exist");
        }
        PropertyRent.update(req.body, {
          where: {
            id: req.params.rentid
          }
        })
          .then((data) => {
            res.status(200).json(data);
          })
          .catch((err) => {
            res.json(err);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});


router.get("/rent/list/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const selectedYear = req.query.year;
        const selectedMonth = req.query.month;
        const totalAmount = await PropertyDetails.sum("rentAmount", {
          where: {
            ownerId: user.uniqueId,
          },
        });
        const emptyAmount = await PropertyDetails.sum("rentAmount", {
          where: {
            availableStatus: 1,
            ownerId: user.uniqueId,
          },
        });
        if (selectedMonth !== "" && selectedYear !== "") {
          PropertyRent.findAll({
            where: {
              month: selectedMonth,
              year: selectedYear,
              ownerId: user.uniqueId,
            },
            include: [{
              model: tenantDetails,
              attributes: ["name", "primaryNumber"],
            },
            {
              model: PropertyDetails,
              as: "property_details",
              attributes: [
                "propertyArea",
                "propertyNumber",
                "floor",
                "bhkType",
                "rentAmount",
              ]
            },
            ],
            order: [
              [
                { model: PropertyDetails, as: "property_details" }, 'propertyNumber', 'ASC'
              ], [
                { model: PropertyDetails, as: "property_details" }, 'floor', 'ASC'
              ]
            ]
          }).then((data) => {
            res.json({
              data: data,
              total_amount: totalAmount,
              empty_amount: emptyAmount,
            });
          });
        } else {
          PropertyRent.findAll({
            where: {
              ownerId: user.uniqueId,
            },
            include: [
              {
                model: PropertyDetails,
                as: "property_details",
                attributes: [
                  "propertyArea",
                  "propertyNumber",
                  "floor",
                  "bhkType",
                  "rentAmount",
                ],
              },
              {
                model: tenantDetails,
                attributes: ["name"],
              },
            ],
            order: ["month"],
          }).then((data) => {
            res.json({
              data: data,
              total_amount: totalAmount,
              empty_amount: emptyAmount,
            });
          });
        }
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.get("/tenant/rent/list/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["tenantJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const rent_details = await PropertyRent.findAll({
          where: {
            tenantId: user.uniqueId,
          },
          order: [
            ["year", "DESC"], ["month", "DESC"]
          ],
        });
        res.json(rent_details)
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

//get Room Agreements
router.get("/agreement/list/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        const agreement_details = await PropertyRentAgreement.findAll({
          where: {
            ownerId: user.uniqueId,
          },
        });
        res.json(agreement_details)
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// details for tenant
router.get("/details/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["tenantJwt"],
    async (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        TenantPropertyLinks.findAll({
          where: {
            tenantId: user.uniqueId
          }
        }).then(async (linkdata) => {
          const activeProperty = linkdata.filter(data => data.status === 'active');
          const propertyDetails = await PropertyDetails.findByPk(activeProperty[0].propertyId);
          const propertyRentHistory = await PropertyRent.findAll({ where: { propertyId: activeProperty[0].propertyId, tenantId: activeProperty[0].tenantId }});
          const stayHistory = await tenantDetails.findByPk(activeProperty[0].tenantId, {
            include: [
              {
                model: PropertyDetails,
                required: false
              }
            ],
            order: [
              [
                { model: tenantDetails.TenantPropertyLinks }, 'status', 'ASC'
              ]
            ]
          });
          res.status(200).json({ ...propertyDetails.dataValues, PropertyRents: [...propertyRentHistory], stayHistory: {...stayHistory.dataValues}, PropertyImages: [] });
        });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});



module.exports = router;
