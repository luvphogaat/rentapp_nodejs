const express = require("express");
const db = require("../models/index");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const passport = require("passport");
const multer = require("multer");
// const tenantList = require('../jsonData/tenantList.json');
require("./passport");
const path = require("path");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

// const resizeImages = async (req, res, next) => {
//     if (!req.files) return next();
//     req.body.images = [];
//     await Promise.all(
//       req.files.map(async file => {
//         const newFilename = '';
//         await sharp(file.buffer)
//           .resize(640, 320)
//           .toFormat("jpeg")
//           .jpeg({ quality: 90 })
//           .toFile(`upload/${newFilename}`);
//         req.body.images.push(newFilename);
//       })
//     );
//     next();
//   };

const imageStorage = multer.diskStorage({
  // Destination to store image
  destination: (req, res, cb) => {
    // const dir = '/home/gangwdoa/gangarent.in/images/userprofile/' + req.params.id;
    const dir =
      "/home/shine/projects/gangaRent/rent_backend/images/police_verification/" +
      req.params.id;
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    } catch (err) {
      console.error(err);
    }
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const imageUpload = multer({
  storage: imageStorage,
  limits: {
    fileSize: 1000000, // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(png|jpg)$/)) {
      // upload only png and jpg format
      return cb(new Error("Please upload a Image"));
    }
    cb(undefined, true);
  },
});

const pdfStorage = multer.diskStorage({
  // Destination to store image
  destination: (req, res, cb) => {
    // const dir = '/home/gangwdoa/gangarent.in/images/userprofile/' + req.params.id;
    const dir =
      "/Users/HOME/Documents/NewFolderProjects/gangaRent/rent_backend/upload/police_verification/" +
      req.params.id;
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    } catch (err) {
      console.error(err);
    }
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const pdfUpload = multer({
  storage: pdfStorage,
  limits: {
    fileSize: 1000000, // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(pdf)$/)) {
      // upload only png and jpg format
      return cb(new Error("Please upload a Pdf"));
    }
    cb(undefined, true);
  },
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const type = file.mimetype.split("/")[1];
    const dir = `./Users/HOME/Documents/NewFolderProjects/gangaRent/rent_backend/upload/${type}`;
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    } catch (err) {
      console.error(err);
    }
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "_" + Date.now() + path.extname(file.originalname)
    );
    // file.fieldname is name of the field (image)
    // path.extname get the uploaded file extension
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/png" || file.mimetype === "application/pdf") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const tenantuploadFilter = (req, file, cb) => {
  if (file.mimetype == "image/*" || file.mimetype == "application/pdf") {
    console.log('p[assing');
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// Set storage engine
const storageS = multer.diskStorage({
  destination: (req, file, cb) => {
    const type = file.mimetype.split("/")[1];
    const dir = `./upload/${type}`;
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    } catch (err) {
      console.error(err);
    }
    console.log('hello',dir)
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

// Init Upload
const upload = multer({
  storage: storageS,
  limits: { fileSize: 2000000 },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  }
}).fields([{name: 'policeVerificationImage'}]);

// Check File Type
const checkFileType = (file, cb) => {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|pdf/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Error: Images and PDF only!');
  }
}
//Database reference
const {
  User,
  tenantDetails,
  PropertyDetails,
  owner_details,
  profilePicture,
  tenantPoliceVerification,
  TenantPropertyLinks,
} = db;

const router = express.Router();

router.get("/list", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        tenantDetails
          .findAll({
            include: [
              {
                model: profilePicture,
                attributes: ["small"],
              },
              {
                model: PropertyDetails,
                required: false,
              },
            ],
          })
          .then((data) => {
            const getFilteredList = data.filter((tenant) => {
              if (tenant.PropertyDetails.length == 0) {
                return tenant;
              } else {
                const propertyLink = tenant.PropertyDetails.every(
                  (tenant) => tenant.TenantPropertyLinks.status == "inactive"
                );
                if (propertyLink === true) {
                  return tenant;
                }
                return;
              }
            });
            tenantDetails
              .findAll({
                include: [
                  {
                    model: profilePicture,
                    attributes: ["small"],
                  },
                  {
                    model: PropertyDetails,
                    required: true,
                  },
                ],
              })
              .then((myTenant) => {
                const getMyTenantFilteredList = myTenant.filter((tenant) => {
                  if (tenant.PropertyDetails.length == 0) {
                    return tenant;
                  } else {
                    const propertyLink = tenant.PropertyDetails.filter(
                      (tenant) => {
                        return (
                          tenant.TenantPropertyLinks.status === "active" &&
                          tenant.TenantPropertyLinks.ownerId == user.uniqueId
                        );
                      }
                    );
                    if (propertyLink.length > 0) {
                      return tenant;
                    }
                    return;
                  }
                  // user.PropertyDetails.length == 0
                });
                res.json({
                  allTenant: getFilteredList,
                  myTenant: getMyTenantFilteredList,
                });
              });
          })
          .catch((e) => {
            res.json(e);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// tenant list of landlord only
router.get("/list/my/", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        tenantDetails
          .findAll({
            include: [
              {
                model: profilePicture,
                attributes: ["small"],
              },
              {
                model: PropertyDetails,
                required: true,
              },
            ],
          })
          .then((myTenant) => {
            const getMyTenantFilteredList = myTenant.filter((user) => {
              if (user.PropertyDetails.length == 0) {
                return user;
              } else {
                const propertyLink = user.PropertyDetails.filter(
                  (tenant) => tenant.TenantPropertyLinks.status == "active"
                );
                if (propertyLink.length > 0) {
                  return user;
                }
                return;
              }
              // user.PropertyDetails.length == 0
            });
            res.json(getMyTenantFilteredList);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// Single Tenant Details
router.get("/list/:id/single", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["ownerJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      // const filteredTenant = tenantList.find(x => x.id == req.params.id);
      // return res.json(filteredTenant);
      try {
        tenantDetails
          .findByPk(req.params.id, {
            include: [
              {
                model: PropertyDetails,
              },
              {
                model: tenantPoliceVerification,
                require: false,
              },
              {
                model: profilePicture,
                attributes: ["small"],
              },
            ],
          })
          .then((data) => {
            res.json(data);
          })
          .catch((e) => {
            res.json(e);
          });
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

router.post(
  "/list/:id/image/profile",
  imageUpload.single("images"),
  async (req, res) => {
    const { filename: image } = req.file;
    try {
      // Small Image of 200 x 200
      // fs.mkdir(req.file.destination + '/small', async (error) => {
      //     await sharp(req.file.path)
      //     .resize(200, 200)
      //     .jpeg({ quality: 90 })
      //     .toFile(
      //         path.resolve(req.file.destination,'small',image)
      //     )
      // });

      // // // thumbnail size for profile
      // fs.mkdir(req.file.destination + '/thumbnail', async (error) => {
      //     await sharp(req.file.path)
      //     .resize(100, 100)
      //     .jpeg({ quality: 90 })
      //     .toFile(
      //         path.resolve(req.file.destination,'thumbnail',image)
      //     )
      // });
      // const profilePictureUrl = 'https://gangarent.in/images/policeVerification/';
      // tenantPoliceVerification.create({
      //     tenantId: req.params.id,
      //     image: profilePictureUrl + req.params.id  + '/' + req.file.filename,
      //     smallImage: profilePictureUrl + req.params.id + '/small/' + req.file.filename,
      //     thumnail: profilePictureUrl + req.params.id + '/thumbnail/' + req.file.filename
      //   })
      //     .then((userNew) => {
      res.send(req.file.filename);
      //     })
    } catch (err) {
      console.log(err);
    }
  },
  (error, req, res, next) => {
    res.status(400).send({ error: error.message });
  }
);

router.post(
  "/new/",
  upload,
  (req, res, next) => {
    passport.authenticate(
      ["ownerJwt"],
      (err, user, info) => {
        if (err) {
          return next(err); // will generate a 500 error
        }
        //   Generate a JSON response reflecting authentication status
        if (!user) {
          return res.status(401).json("Unauthorized");
        }
        // const filteredTenant = tenantList.find(x => x.id == req.params.id);
        // return res.json(filteredTenant);
        try {
          console.log("hi here");
          return res.send(req.body);
          // tenantDetails
          //   .create({
          //     id: uuidv4(),
          //     name: req.body.name,
          //     plan: "free",
          //     permanentAddress: req.body.permanentaddress,
          //     primaryNumber: req.body.mobilenumber,
          //     primaryNumberPrefix: "91",
          //     secondaryNumber: req.body.secondaryNumber,
          //     secondaryNumberPrefix: "91",
          //     gender: req.body.gender,
          //   })
          //   .then((data) => {
          // tenantPoliceVerification
          //   .create({
          //     tenantId: data.id,
          //     image: "12",
          //     smallImage: "12",
          //     thumnail: "12",
          //     verificationNumber: req.body.policeverificationnumber,
          //     latest: true,
          //   })
          //   .then((police) => {})
          //   .catch((err) => {});
          // if (req.body.assignProperty == "Yes") {
          //   TenantPropertyLinks.create({
          //     id: uuidv4(),
          //     propertyId: req.body.roomid,
          //     tenantId: data.id,
          //     startDate: new Date(),
          //     endDate: null,
          //     status: "active",
          //   })
          //     .then((link) => {})
          //     .catch((err) => {});
          // }
          // if (req.body.profilePicture && req.body.profilePicture != "") {
          //   profilePicture
          //     .create({
          //       userId: data.id,
          //       image: "12",
          //       small: "12",
          //       thumnail: "12",
          //     })
          //     .then((picture) => {})
          //     .catch((err) => {});
          // }
          // res.status(201).json(user);
          // })
          // .catch((err) => {
          //   res.json(err);
          // });
        } catch (e) {
          res.json(e);
        }
      },
      { session: false }
    )(req, res, next);
  }
);

// loggedin Tenant Dashboard
router.get("/dashboard", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["tenantJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      // const filteredTenant = tenantList.find(x => x.id == req.params.id);
      // return res.json(filteredTenant);
      try {
        if(user.uniqueId === '') {
          const jsonData = {
            name: user.firstName + ' ' + user.lastName,
            complaintCount: 0,
            policeVerStatus: "",
            rentStatus: "",
          };
          res.json(jsonData);
        } else {
          tenantDetails
          .findByPk(user.uniqueId, {
            include: [
              {
                model: PropertyDetails,
              },
              {
                model: tenantPoliceVerification,
                require: false,
              },
              {
                model: profilePicture,
                attributes: ["small"],
              },
            ],
          })
          .then((data) => {
            const jsonData = {
              name: data.name,
              gender: data.gender,
              complaintCount: 0,
              policeVerStatus: "Active",
              rentStatus: "Pending",
            };
            // const combineJson = Object.assign({}, data, jsonData);
            res.json(jsonData);
          })
          .catch((e) => {
            res.json(e);
          });
        }
        
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

// police-verification
router.get("/police-verification", jsonParser, (req, res, next) => {
  passport.authenticate(
    ["tenantJwt"],
    (err, user, info) => {
      if (err) {
        return next(err); // will generate a 500 error
      }
      //   Generate a JSON response reflecting authentication status
      if (!user) {
        return res.status(401).json("Unauthorized");
      }
      try {
        tenantPoliceVerification.findOne({
          where: {
            tenantId: user.uniqueId
          }
        }).then(data =>{
          console.log(user.uniqueId);
          res.json(data);
        })
      } catch (e) {
        res.json(e);
      }
    },
    { session: false }
  )(req, res, next);
});

module.exports = router;
