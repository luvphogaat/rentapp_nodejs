// const express = require("express");
// // const url = require('url');
// const db = require("../models/index");
// const bodyParser = require("body-parser");
// const jsonParser = bodyParser.json();
// const passport = require("passport");
// const roomList = require("../jsonData/roomList.json");
// const { v4: uuidv4 } = require("uuid");
// require("./passport");
// const env = process.env.NODE_ENV || "development";
// const roomSearchFilters = require("../jsonData/roomSearchCreteriaFilter.json");
// const config = require(__dirname + "/../config/config.json");
// // var AWS = require("aws-sdk");
// // AWS.config.loadFromPath(__dirname + "/../config/config.json");

// //Database reference
// const {
//   owner_details,
//   PropertyDetails,
//   PropertyImages,
//   tenantDetails,
//   TenantPropertyLinks,
//   PropertyRent,
//   SmsTemplate
// } = db;

// const smsTemplates = {
//   rentreminder: {
//     name: "rentReminder",
//     message: `Hi :tenantName, This is a reminder for the rent for the month of November, 2022 for HNo. :propertyNumber, :propertyArea, :propertyFloor floor. If already paid kindly ignore this message.
//             From Landlord`,
//   },
// };

// const router = express.Router();

// router.get("/rent/reminder/:propertId/:tenantId", async (req, res) => {
//   // Create SMS Attribute parameter you want to get  
//   try {
//     const smsObj = req.body.smsObj;
//     const smsMessage = req.body.template;
//     smsObj.forEach(element => {
//       var params = {
//         Message: smsMessage,
//         PhoneNumber: element.primaryNumber,
//       };
  
//       // Create promise and SNS service object
//       var publishTextPromise = new AWS.SNS({ apiVersion: "latest" })
//         .publish(params)
//         .promise();
//       // Handle promise's fulfilled/rejected states
//       publishTextPromise
//         .then(function (data) {
//           console.log("MessageID is " + data.MessageId);
//         })
//         .catch(function (err) {
//           console.error(err, err.stack);
//         });
//     });
//     res.json(data);
//   } catch (e) {
//     res.json(e);
//   }
// });


// router.post("/send/", jsonParser, (req, res, next) => {
//   passport.authenticate(
//     ["ownerJwt"],
//     async (err, user, info) => {
//       if (err) {
//         return next(err); // will generate a 500 error
//       }
//       //   Generate a JSON response reflecting authentication status
//       if (!user) {
//         return res.status(401).json("Unauthorized");
//       }
//       try {
//         const sendArray = [];
//         const smsObj = req.body.smsObj;
//         const smsMessage = req.body.template;
//         var sns = new AWS.SNS({ apiVersion: "latest"});
//         sns.setSMSAttributes(
//           {
//               attributes: {
//                   DefaultSMSType: "Transactional"
//               }
//           },
//           function (error) {
//               if (error) {
//                   console.log(error);
//               }
//           }
//       );
//         smsObj.forEach(element => {
//           var params = {
//             Message: smsMessage,
//             PhoneNumber: element.primaryNumberPrefix + element.primaryNumber,
//           };
//           // Create promise and SNS service object
//           sns
//             .publish(params)
//             .promise()
//             .then(function (data) {
//               console.log("MessageID is " + data.MessageId, params);
//               sendArray.push({
//                 paramsData: params,
//                 smsData: data              
//               });
//             })
//             .catch(function (err) {
//               console.error(err, err.stack);
//             });
//         });
//         console.log(sendArray);
//         res.json(sendArray);
//       } catch (e) {
//         res.json(e);
//       }
//     },
//     { session: false }
//   )(req, res, next);
// });


// router.get("/rent/templates/", jsonParser, (req, res, next) => {
//   passport.authenticate(
//     ["ownerJwt"],
//     async (err, user, info) => {
//       if (err) {
//         return next(err); // will generate a 500 error
//       }
//       //   Generate a JSON response reflecting authentication status
//       if (!user) {
//         return res.status(401).json("Unauthorized");
//       }
//       try {
//         const templateList = await SmsTemplate.findAll({
//           where: {
//             userId: user.uniqueId
//           }
//         });
//         res.status(200).json(templateList);
//       } catch (e) {
//         res.json(e);
//       }
//     },
//     { session: false }
//   )(req, res, next);
// });

// module.exports = router;
