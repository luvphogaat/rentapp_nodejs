const db = require('../models/index')
const {User} = db;
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const saltRounds = 10;
const { secretKey } = require('../config/config.json');

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = secretKey;

passport.use(
    'tenantJwt',
    new JwtStrategy(opts, (jwtPayload, done) => {
      User.findOne({ where: { id: jwtPayload.id, type: 'tenant' }, raw: false })
        .then((user) => {
          if (!user) {
            return done(null, false, { message: 'Incorrect user.' });
          }
          return done(null, user);
        })
        .catch((err) => (null, false, err));
    })
);

passport.use(
    'ownerJwt',
    new JwtStrategy(opts, (jwtPayload, done) => {
      User.findOne({ where: { id: jwtPayload.id, type: 'owner' }, raw: false })
        .then((user) => {
          if (!user) {
            return done(null, false, { message: 'Incorrect user.' });
          }
          return done(null, user);
        })
        .catch((err) => (null, false, err));
    })
);

passport.use(
    'guestJwt',
    new JwtStrategy(opts, (jwtPayload, done) => {
      User.findOne({ where: { id: jwtPayload.id, type: 'owner' }, raw: false })
        .then((user) => {
          if (!user) {
            return done(null, false, { message: 'Incorrect user.' });
          }
          return done(null, user);
        })
        .catch((err) => (null, false, err));
    })
);