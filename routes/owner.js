const express = require('express');
const db = require('../models/index')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const passport = require('passport');
const ownerList = require('../jsonData/ownerList.json');
const propertyList = require('../jsonData/propertyList.json');
require('./passport');


//Database reference
// const {User} = db;

const router = express.Router();

router.get('/list', jsonParser, (req, res, next) => {
    passport.authenticate(['ownerJwt'], (err, user, info) => {
        if (err) {
            return next(err); // will generate a 500 error
          }
        //   Generate a JSON response reflecting authentication status
        if (!user) {
            return res.status(401).json('Unauthorized');
        }
        res.json(ownerList);
    }, {session: false})(req, res, next);
});



router.get('/property/list/', jsonParser, (req, res, next) => {
    passport.authenticate(['ownerJwt'], (err, user, info) => {
        if (err) {
            return next(err); // will generate a 500 error
        }
        //   Generate a JSON response reflecting authentication status
        if (!user) {
            return res.status(401).json('Unauthorized');
        }
        return res.json(propertyList);
    }, {session: false})(req, res, next);
});

// Single Property Details
router.get('/property/list/:id', jsonParser, (req, res, next) => {
    console.log(req.header)
    passport.authenticate(['ownerJwt'], (err, user, info) => {
        if (err) {
            return next(err); // will generate a 500 error
        }
        //   Generate a JSON response reflecting authentication status
        if (!user) {
            return res.status(401).json('Unauthorized');
        }
        const filteredProperty = propertyList.find(x => x.id == req.params.id);
        return res.json(filteredProperty);
    }, {session: false})(req, res, next);
});



module.exports = router;