const express = require("express");
// const url = require('url');
const db = require("../models/index");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const passport = require("passport");
const roomList = require("../jsonData/roomList.json");
const { v4: uuidv4 } = require("uuid");
require("./passport");
const roomSearchFilters = require("../jsonData/roomSearchCreteriaFilter.json");

//Database reference
const {
  owner_details,
  PropertyDetails,
  PropertyImages,
  tenantDetails,
  TenantPropertyLinks,
  PropertyRent,
} = db;

const router = express.Router();


router.get("/", jsonParser, (req, res, next) => {
    passport.authenticate(
      ["ownerJwt"],
      async (err, user, info) => {
        if (err) {
          return next(err); // will generate a 500 error
        }
        //   Generate a JSON response reflecting authentication status
        if (!user) {
          return res.status(401).json("Unauthorized");
        }
        try {
            const totalAmount = await PropertyDetails.sum("rentAmount", {
                where: {
                  ownerId: user.uniqueId,
                },
            });
            const totalProperty = await PropertyDetails.count({
                where: {
                  ownerId: user.uniqueId,
                },
            });
            const vacatedProperty = await PropertyDetails.count({
                where: {
                  ownerId: user.uniqueId,
                  availableStatus: 1
                },
            });

            const property_tenant = await TenantPropertyLinks.count({
              where: {
                ownerId : user.uniqueId,
                status: 'active'
              }
            });
            const today = new Date()
            const collectAmount  = await PropertyRent.sum("rentAmount", {
              where: {
                ownerId: user.uniqueId,
                month: today.getMonth() + 1,
                year: today.getFullYear()
              }
            });
            const pendingAmount = totalAmount - collectAmount;
            const dataArray = [
                {
                    text: 'Total Property',
                    value: totalProperty,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                },  {
                    text: 'Total Vacated',
                    value: vacatedProperty,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                },  {
                    text: 'Active Tenants',
                    value: property_tenant,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                },  {
                    text: 'Rent Collected (INR)',
                    value: collectAmount ? collectAmount : 0,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                },  {
                    text: 'Total Rent (INR)',
                    value: totalAmount,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                }, {
                    text: 'Total Issues',
                    value: 0,
                    icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==',
                }, {
                  text: 'Pending Rent (INR)',
                  value: pendingAmount,
                  icon: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw=='
                }
            ]
            res.json(dataArray);
        } catch (e) {
          res.json(e);
        }
      },
      { session: false }
    )(req, res, next);
  });


  module.exports = router;