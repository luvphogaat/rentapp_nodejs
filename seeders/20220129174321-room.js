'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('PropertyDetails', [
      {
      "id": "d3254c8a-1513-42e5-b217-36df318bd637",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Ground",
      "roomNumber": 1,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Private",
      "electricityPerUnit": "7",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 1,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "4c583bbe-806a-4fb0-a81e-36443633b2ef",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "First",
      "roomNumber": 2,
      "purpose": "Residential",
      "bhkType": "1BHK",
      "rentAmount": 12600,
      "depositAmount": 7000,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "8d1e6c2c-178b-4324-87a9-fbef5b6b5666",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "First",
      "roomNumber": 3,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7000,
      "depositAmount": 7000,
      "water": 0,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "8f709d32-2bdd-444e-ad8a-577747978a6b",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Second",
      "roomNumber": 4,
      "purpose": "Residential",
      "bhkType": "1BHK",
      "rentAmount": 12500,
      "depositAmount": 12500,
      "water": 0,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "4aa71634-936f-49a4-af0e-0888a17c24cd",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Second",
      "roomNumber": 5,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 0,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "fc226ff0-8ee0-4a83-9a33-9f0434d40be7",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Third",
      "roomNumber": 6,
      "purpose": "Residential",
      "bhkType": "1BHK",
      "rentAmount": 13000,
      "depositAmount": 10000,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "d058ece9-93b9-4d18-9f77-d78788fd7e0c",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Third",
      "roomNumber": 7,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 0,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "104333c2-35b5-453d-80b5-4946c1673fdd",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Fourth",
      "roomNumber": 8,
      "purpose": "Residential",
      "bhkType": "1BHK",
      "rentAmount": 13000,
      "depositAmount": 12300,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Private",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "946c8bbf-b50d-4de2-b002-2a61638fd90f",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Fourth",
      "roomNumber": 9,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 0,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "b9491c01-ee28-4be2-839a-1a66e2717c0f",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Fifth",
      "roomNumber": 10,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 8000,
      "depositAmount": 8000,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Govt",
      "electricityPerUnit": "",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 0,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  }, {
      "id": "1b3e42a7-3268-4022-bd99-fe3e98b3a018",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Himayun Pur",
      "propertyNumber": "92",
      "floor": "Fifth",
      "roomNumber": 11,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 300,
      "maintenance": 100,
      "maintenanceType": "monthly",
      "electricityType": "Private",
      "electricityPerUnit": "7",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 0,
      "availableStatus": 1,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
  } , {
      "id": "91f76244-f818-4912-b585-9cb16e1b9f08",
      "ownerId": "5a8d028b-61c8-46e2-8b4a-94bd0529c4ae",
      "locationArea": "Safdarjung Enclave",
      "propertyArea": "Krishna Nagar",
      "propertyNumber": "87/1",
      "floor": "First",
      "roomNumber": 4,
      "purpose": "Residential",
      "bhkType": "1RK",
      "rentAmount": 7500,
      "depositAmount": 7500,
      "water": 300,
      "maintenance": 0,
      "maintenanceType": "monthly",
      "electricityType": "Private",
      "electricityPerUnit": "7",
      "furnishedStatus": "Unfurnished",
      "category": "Flats",
      "bathroom": 1,
      "kitchen": 1,
      "balcony": 1,
      "availableStatus": 1,
      "availableDate": "",
      "society": false,
      "district": "South Delhi",
      "state": "Delhi",
      "createdAt": new Date(),
      "updatedAt": new Date()
    }
  ]);
},

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
