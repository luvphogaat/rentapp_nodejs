'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('tenantDetails', [
      {
        id: '2a49e27c-8412-48f4-ac03-6960e6434d0a',
        name: 'Japfiio Kayina',
        plan: 'free',
        permanentAddress: 'Village Tadubi Bazar, P.S. Tadubi, District Senapati Dist, Manipur',
        primaryNumber: '9717678153',
        primaryNumberPrefix: '91',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: 'male',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        id: '3eb0d4be-9a96-4981-95bf-9ce876c552f9',
        name: 'Bharat Singh Negi',
        plan: 'free',
        permanentAddress: 'HNO-B-2/23, Ground Floor, Near St Marys School, Safdarjung Enclave, New Delhii - 110029',
        primaryNumber: '8826131027',
        primaryNumberPrefix: '91',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: 'male',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        id: 'abff9b1c-5d95-4a0d-a3be-bb557e02939f',
        name: 'Rubi Debbarma',
        plan: 'free',
        permanentAddress: 'HNO 125, Maharani, PS. Salema, Dist Dhalai, Tripura',
        primaryNumber: '9873297411',
        primaryNumberPrefix: '91',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: 'female',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        id: '6ad92a90-2e3b-4b96-9487-f1156e17c1ae',
        name: 'Kiyeshi Zhimo',
        plan: 'free',
        permanentAddress: 'Amutasah, Chekiye, Dimapur, Nagaland, 797115',
        primaryNumber: '7005972034',
        primaryNumberPrefix: '91',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: 'female',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        id: '905867ad-d535-4787-90c3-0960118a6d63',
        name: 'Satish Chander',
        plan: 'free',
        permanentAddress: 'A-2/57  1st Floor, Safdarjung Enclave, New Delhi - 29',
        primaryNumber: '26182465',
        primaryNumberPrefix: '011',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: '',
        createdAt: new Date(),
        updatedAt: new Date()
      } ,{
        id: 'e054b016-edec-4111-9ec9-463cf6c937cb',
        name: 'Avinash Mandal',
        plan: 'free',
        permanentAddress: 'Village Kola Kushma Koradih, District Dhanbad, Jharkhand',
        primaryNumber: '9899151305',
        primaryNumberPrefix: '91',
        secondaryNumber: '',
        secondaryNumberPrefix: '',
        gender: 'male',
        createdAt: new Date(),
        updatedAt: new Date()
      }
     ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
