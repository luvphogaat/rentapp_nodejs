'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('SmsTemplates', [
      {
        id: 'fa2bbd7b-16d0-496e-94f3-6b371c32acc7',
        templateName: 'Hello! This is just a reminder that rent is due in a few days. If paid kindly ignore. Thank you for your attention to this matter. From Landlord',
        userId: '5a8d028b-61c8-46e2-8b4a-94bd0529c4ae',
        createdAt: new Date(),
        updatedAt: new Date()
      }]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
