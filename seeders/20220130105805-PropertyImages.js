'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('PropertyImages', [
       {
          id: "c57e0444-995d-4517-9b73-731ca511a3bc",
          propertyId: "d3254c8a-1513-42e5-b217-36df318bd637",
          image: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/room.jpeg",
          smallImage: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/room.jpeg",
          thumbnail: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/room.jpeg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "aaa1db3f-669b-47de-a4fe-07c129ce5c2d",
          propertyId: "d3254c8a-1513-42e5-b217-36df318bd637",
          image: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/kitchen.jpeg",
          smallImage: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/kitchen.jpeg",
          thumbnail: "https://gangarent.in/images/d3254c8a-1513-42e5-b217-36df318bd637/kitchen.jpeg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "0c85d488-803f-4dfc-91ee-27adf84b0cbf",
          propertyId: "1b3e42a7-3268-4022-bd99-fe3e98b3a018",
          image: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/room.jpeg",
          smallImage: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/room.jpeg",
          thumbnail: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/room.jpeg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "022b37ce-a054-4677-a432-b33b14607496",
          propertyId: "1b3e42a7-3268-4022-bd99-fe3e98b3a018",
          image: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/kitchen.jpeg",
          smallImage: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/kitchen.jpeg",
          thumbnail: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/kitchen.jpeg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "6994fd8a-c7c8-41b0-bdf8-ac85b7986a32",
          propertyId: "1b3e42a7-3268-4022-bd99-fe3e98b3a018",
          image: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/bathroom.jpeg",
          smallImage: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/bathroom.jpeg",
          thumbnail: "https://gangarent.in/images/1b3e42a7-3268-4022-bd99-fe3e98b3a018/bathroom.jpeg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "6ad7440e-549e-41b4-b724-02f33e5e6db3",
          propertyId: "91f76244-f818-4912-b585-9cb16e1b9f08",
          image: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a6a6cee5.jpg",
          smallImage: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a6a6cee5.jpg",
          thumbnail: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a6a6cee5.jpg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "3a19efc3-61f3-426a-bcf2-5c4b8ed8b6ca",
          propertyId: "91f76244-f818-4912-b585-9cb16e1b9f08",
          image: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a61db991.jpg",
          smallImage: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a61db991.jpg",
          thumbnail: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a61db991.jpg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "119ae163-14a0-4629-bdde-dea0cca6d50d",
          propertyId: "91f76244-f818-4912-b585-9cb16e1b9f08",
          image: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a68a8c2c.jpg",
          smallImage: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a68a8c2c.jpg",
          thumbnail: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f929a68a8c2c.jpg",
          createdAt: new Date(),
          updatedAt: new Date()
       }, {
          id: "7217567f-d596-43d7-a4cc-9984c8f4f98c",
          propertyId: "91f76244-f818-4912-b585-9cb16e1b9f08",
          image: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f9298a163601.jpg",
          smallImage: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f9298a163601.jpg",
          thumbnail: "https://gangarent.in/images/91f76244-f818-4912-b585-9cb16e1b9f08/room-5f9298a163601.jpg",
          createdAt: new Date(),
          updatedAt: new Date()
       }
     ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
