'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PropertyRentAgreement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyRentAgreement.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: DataTypes.UUID,
    propertyId: DataTypes.UUID,
    tenantId: DataTypes.UUID,
    certificateNumber: DataTypes.STRING,
    certificateDateTime: DataTypes.STRING,
    FirstParty: DataTypes.STRING,
    secondParty: DataTypes.STRING,
    startDate: DataTypes.DATE,
    endDate: DataTypes.DATE,
    image: DataTypes.TEXT,
  }, {
    sequelize,
    modelName: 'PropertyRentAgreement',
  });
  return PropertyRentAgreement;
};