'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tenantDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  tenantDetails.init({
    name: DataTypes.STRING,
    plan: DataTypes.STRING,
    permanentAddress: DataTypes.TEXT,
    primaryNumber: DataTypes.STRING,
    primaryNumberPrefix: DataTypes.STRING,
    secondaryNumber: DataTypes.STRING,
    secondaryNumberPrefix: DataTypes.STRING,
    gender: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tenantDetails',
  });

  tenantDetails.associate = function(models) {
    tenantDetails.hasMany(models.tenantPoliceVerification, {  foreignKey: 'tenantId'  });
    tenantDetails.hasOne(models.profilePicture, {  foreignKey: 'userId'  });
    tenantDetails.hasMany(models.PropertyRent, { foreignKey: 'tenantId' });
    tenantDetails.hasMany(models.PropertyElectricityBills, {foreignKey: 'tenantId'});
    tenantDetails.hasMany(models.PropertyMiscBills, {foreignKey: 'tenantId'});
    tenantDetails.belongsToMany(models.PropertyDetails, {
      through: models.TenantPropertyLinks,
      foreignKey: 'tenantId'
    });
  }
  
  return tenantDetails;
};