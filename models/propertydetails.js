'use strict';
const {
  Model
} = require('sequelize');

const owner_details = require ('./owner_details');
module.exports = (sequelize, DataTypes) => {
  class PropertyDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyDetails.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: {
      type:DataTypes.STRING,
      foreignKey: true
    },
    locationArea: DataTypes.STRING,
    propertyArea: DataTypes.STRING,
    propertyNumber: DataTypes.STRING,
    floor: DataTypes.INTEGER,
    roomNumber: DataTypes.INTEGER,
    purpose: DataTypes.STRING,
    bhkType: DataTypes.STRING,
    rentAmount: DataTypes.INTEGER,
    depositAmount: DataTypes.INTEGER,
    water: DataTypes.INTEGER,
    maintenance: DataTypes.INTEGER,
    maintenanceType: DataTypes.STRING,
    electricityType: DataTypes.STRING,
    electricityPerUnit: DataTypes.STRING,
    furnishedStatus: DataTypes.STRING,
    category: DataTypes.STRING,
    bathroom: DataTypes.INTEGER,
    kitchen: DataTypes.INTEGER,
    balcony: DataTypes.INTEGER,
    availableStatus: DataTypes.INTEGER,
    availableDate: DataTypes.STRING,
    society: DataTypes.BOOLEAN,
    district: DataTypes.STRING,
    state: DataTypes.STRING,
    // visible: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'PropertyDetails',
  });

  PropertyDetails.associate = function(models) {
    PropertyDetails.belongsTo(models.owner_details, {  foreignKey: 'ownerId'  });
    PropertyDetails.hasMany(models.PropertyImages, {  foreignKey: 'propertyId'  });
    PropertyDetails.hasMany(models.PropertyRent, { foreignKey: 'propertyId' });
    PropertyDetails.hasMany(models.PropertyElectricityBills, { foreignKey: 'propertyId' });
    PropertyDetails.hasMany(models.PropertyMiscBills, { foreignKey: 'propertyId' });
    // PropertyDetails.hasMany(models.TenantPropertyLinks, {foreignKey: 'propertyId'});
    PropertyDetails.belongsToMany(models.tenantDetails, { 
      through: models.TenantPropertyLinks,
      foreignKey: 'propertyId'
    });
  }
  



  return PropertyDetails;
};