'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PropertyRent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyRent.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: DataTypes.UUID,
    propertyId: DataTypes.UUID,
    tenantId: DataTypes.UUID,
    rentAmount: DataTypes.DOUBLE,
    balance: DataTypes.DOUBLE,
    advance: DataTypes.DOUBLE,
    month: DataTypes.STRING,
    year: DataTypes.INTEGER,
    pay_mode: DataTypes.STRING,
    pay_dest: DataTypes.STRING,
    pay_status: DataTypes.ENUM(["PAID", "UNPPAID"]),
  }, {
    sequelize,
    modelName: 'PropertyRent',
  });

  PropertyRent.associate = function(models) {
    PropertyRent.belongsTo(models.PropertyDetails, {as: 'property_details',  foreignKey: 'propertyId'  });
    PropertyRent.belongsTo(models.tenantDetails, {foreignKey: 'tenantId'});
  }
  return PropertyRent;
};