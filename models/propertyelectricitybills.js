'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PropertyElectricityBills extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyElectricityBills.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: DataTypes.UUID,
    propertyId: DataTypes.UUID,
    tenantId: DataTypes.UUID,
    oldReading: DataTypes.INTEGER,
    newReading: DataTypes.INTEGER,
    totalUnits: DataTypes.INTEGER,
    billAmount: DataTypes.DOUBLE,
    month: DataTypes.INTEGER,
    year: DataTypes.INTEGER,
    pay_mode: DataTypes.STRING,
    pay_dest: DataTypes.STRING,
    pay_status: DataTypes.ENUM(["PAID", "UNPPAID"]),
  }, {
    sequelize,
    modelName: 'PropertyElectricityBills',
  });
  PropertyElectricityBills.associate = function(models) {
    PropertyElectricityBills.belongsTo(models.PropertyDetails, {as: 'property_details',  foreignKey: 'propertyId'  });
    PropertyElectricityBills.belongsTo(models.tenantDetails, {foreignKey: 'tenantId'});
  }
  return PropertyElectricityBills;
};