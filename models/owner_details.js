'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class owner_details extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  owner_details.init({
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: DataTypes.STRING,
    aadharCard: DataTypes.STRING,
    fatherName: DataTypes.STRING,
    primaryNumber: DataTypes.STRING,
    secondaryNumber: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'owner_details',
  });


  owner_details.associate = function(models) {
    owner_details.hasOne(models.profilePicture, {  foreignKey: 'userId'  });
    owner_details.hasMany(models.PropertyDetails, {  
      foreignKey: 'id'
    });
  }

  return owner_details;
};