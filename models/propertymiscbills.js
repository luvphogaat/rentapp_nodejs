'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PropertyMiscBills extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyMiscBills.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: DataTypes.UUID,
    propertyId: DataTypes.UUID,
    tenantId: DataTypes.UUID,
    type: DataTypes.ENUM(["WATER", "CLEANING"]),
    month: DataTypes.INTEGER,
    year: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    pay_mode: DataTypes.ENUM(["CASH", "ONLINE"]),
    pay_dest: DataTypes.STRING,
    pay_status: DataTypes.ENUM(["PAID", "UNPPAID"]),
  }, {
    sequelize,
    modelName: 'PropertyMiscBills',
  });

  PropertyMiscBills.associate = function(models) {
    PropertyMiscBills.belongsTo(models.PropertyDetails, {as: 'property_details',  foreignKey: 'propertyId'  });
    PropertyMiscBills.belongsTo(models.tenantDetails, {foreignKey: 'tenantId'});
  }
  return PropertyMiscBills;
};