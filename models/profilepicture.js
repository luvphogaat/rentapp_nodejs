'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class profilePicture extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  profilePicture.init({
    userId: DataTypes.UUID,
    image: DataTypes.TEXT,
    small: DataTypes.TEXT,
    thumbnail: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'profilePicture',
  });

  profilePicture.associate = function(models) {
    profilePicture.hasOne(models.tenantDetails, {foreignKey: 'id'});
    profilePicture.hasOne(models.owner_details, {foreignKey: 'id'});
  }
  return profilePicture;
};