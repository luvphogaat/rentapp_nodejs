'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PropertyImages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  PropertyImages.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    propertyId: {
      type: DataTypes.UUID,
      foreignKey: true
    },
    image: DataTypes.TEXT,
    smallImage: DataTypes.TEXT,
    thumbnail: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'PropertyImages',
  });

  PropertyImages.associate = function(models) {
    PropertyImages.hasMany(models.PropertyDetails, {  foreignKey: 'id'  });
  }

  return PropertyImages;
};