'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tenantPoliceVerification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  tenantPoliceVerification.init({
    tenantId: DataTypes.UUID,
    image: DataTypes.TEXT,
    smallImage: DataTypes.TEXT,
    thumnail: DataTypes.TEXT,
    verificationNumber: DataTypes.STRING,
    latest: DataTypes.ENUM(["true","false"])
  }, {
    sequelize,
    modelName: 'tenantPoliceVerification',
  });

  tenantPoliceVerification.associate = function(models) {
    tenantPoliceVerification.hasMany(models.tenantDetails, {  foreignKey: 'id'  });
  }


  return tenantPoliceVerification;
};