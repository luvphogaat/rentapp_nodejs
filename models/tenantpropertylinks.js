'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TenantPropertyLinks extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  TenantPropertyLinks.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    ownerId: DataTypes.UUID,
    propertyId: DataTypes.UUID,
    tenantId: DataTypes.UUID,
    startDate: DataTypes.DATE,
    endDate: DataTypes.DATE,
    comment: DataTypes.TEXT,
    rent_amount: DataTypes.INTEGER,
    security: DataTypes.INTEGER,
    security_deposit_amount: DataTypes.INTEGER,
    security_amount_date: DataTypes.DATE,
    security_return_amount: DataTypes.INTEGER,
    security_return_date: DataTypes.DATE,
    pay_date: DataTypes.INTEGER,
    status: DataTypes.ENUM(["active","inactive"])
  }, {
    sequelize,
    modelName: 'TenantPropertyLinks',
  });

  return TenantPropertyLinks;
};